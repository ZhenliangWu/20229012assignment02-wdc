const React = require('react');
const ReactRedux = require('react-redux');
const _ = require('lodash');

const createActionDispatchers = require('../helpers/createActionDispatchers');
const notesActionCreators = require('../reducers/notes');

const api = require('../helpers/api');

const NoteDetail = require('../components/NoteDetail');
const MarkdownEditor = require('../components/MarkdownEditor');

class NoteList extends React.Component {
  constructor(props) {
    super(props);

    console.log('NoteList', this);

    this.previousNotebookId = null;

    this.state = {
      isCreating: false,
      noteId: null,
      note: {
        title: '',
        content: '',
      },
      search: '',
    }
  }

  componentDidMount() {
    console.log('NoteList componentDidMount', this);

  }

  loadNoteList() { 
    if (this.props.notebookId !== null) {
      return api.get('/notebooks/'+this.props.notebookId+'/notes').then(res => {
        this.props.list_notes(res);
      });
    }
  }

  componentDidUpdate() {
    console.log('NoteList componentDidUpdate', this);

    if (this.previousNotebookId !== this.props.notebookId) {
      this.previousNotebookId = this.props.notebookId;
      this.loadNoteList();
    }
  }

  displayNote(id) {
    console.log('displayNote', id);
    this.setState({
      noteId: id,
    });
  }

  showCreateNote(e) {
    this.setState({
      isCreating: true,
    })
  }
  hideCreateNote(e) {
    this.setState({
      isCreating: false,
    })
  }

  deleteNote(id) {
    api.delete('/notes/'+id).then(res => {
      this.loadNoteList();
    });
    if (id === this.state.noteId) {
      this.displayNote(null);
    }
  }

  onChangeNoteTitle(e) {
    const note = _.assign({}, this.state.note);
    note.title = e.target.value;
    this.setState({
      note: note,
    });
  }
  onChangeNoteContent(e) {
    const note = _.assign({}, this.state.note);
    note.content = e.target.value;
    this.setState({
      note: note,
    });
  }
  createNote(e) {
    api.post('/notes', {
      title: this.state.note.title,
      content: this.state.note.content,
      notebookId: this.props.notebookId,
    }).then(res => {
      this.hideCreateNote();
      this.loadNoteList();
    });

    e.preventDefault();
  }

  onNoteSearchChange(e) {
    console.log(this, e);
    this.setState({
      search: e.target.value
    });
  }
  
  searchNotes(e) {
    api.get('/notebooks/'+this.props.notebookId+'/notes?s=' + this.state.search).then(res => {
      this.props.list_notes(res);
    });
    
    e.preventDefault();
  }

  render() {
    const createNoteListItem = (note) => {
      return (
        <li key={note.id} className="my-note-list">
          <button className="btn btn-danger btn-xs" onClick={() => this.deleteNote(note.id)}><i className="fa fa-close"></i></button>
          <a className="my-title" onClick={() => this.displayNote(note.id)}>{note.title}</a>
        </li>
      )
    }

    if (null === this.props.notebookId) {
        return null;
    }

    return (
      <div>
        <h3>Notes</h3>
        
        <div className="my-search row">
          <form className="form-inline">
            <div className="form-group my-search-input-wrap">
              <input type="text" className="form-control my-search-input" id="noteSearch" placeholder="Search notes" value={this.state.search} onChange={(e) => this.onNoteSearchChange(e)} />
            </div>          
            <button onClick={(e) => this.searchNotes(e)} className="btn btn-primary">Search</button>
          </form>
        </div>

        {!this.state.isCreating ? <button className="btn btn-primary" onClick={(e) => this.showCreateNote(e)}><i className="fa fa-plus"></i> New note</button>
        : 
        <div className="row">
          <form className="form col-sm-5">
            <input type="text" className="form-control" id="noteTitle" placeholder="Note title" value={this.state.note.title} onChange={(e) => this.onChangeNoteTitle(e)} />
            <MarkdownEditor value={this.state.note.content} onChange={(e) => this.onChangeNoteContent(e)} />

            <button className="btn btn-success" onClick={(e) => this.createNote(e)}><i className="fa fa-check"></i></button>
            <button className="btn btn-default" onClick={(e) => this.hideCreateNote(e)}>Cancel</button>
          </form>
        </div>
        }

        <ul>
          {this.props.notes.data.map(createNoteListItem)}
        </ul>
        
        {/* <div className="row">
          <div className="col-sm-6">
            <ul className="list-group">
              {this.props.notes.data.map(createNoteListItem)}
            </ul>
          </div>
          
          <div className="col-sm-6">
            <form className="form">
              <div className="form-group">
                <label htmlFor="noteTitle">Note title</label>
                <input type="text" className="form-control" id="noteTitle" placeholder="Note title" value={this.state.note.title} onChange={(e) => this.onChangeNoteTitle(e)} />
              </div>
              <div className="form-group">
                <label htmlFor="noteContent">Note content</label>
                <MarkdownEditor value={this.state.note.content} onChange={(e) => this.onChangeNoteContent(e)} />
              </div>
              
              <div className="form-group">
                <button onClick={(e) => this.createNote(e)} className="btn btn-primary">Create new note</button>
              </div>
            </form>
          </div>
          
        </div> */}

        <NoteDetail noteId={this.state.noteId} />
      </div>
    );
  }
}

const NoteListContainer = ReactRedux.connect(
  state => ({
    notes: state.notes
  }),
  createActionDispatchers(notesActionCreators)
)(NoteList);

module.exports = NoteListContainer;
