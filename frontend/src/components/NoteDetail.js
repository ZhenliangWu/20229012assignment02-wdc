const React = require('react');
const ReactRedux = require('react-redux');

const createActionDispatchers = require('../helpers/createActionDispatchers');
const notesActionCreators = require('../reducers/notes');

const api = require('../helpers/api');

class NoteDetail extends React.Component {
  constructor(props) {
    super(props);

    this.previousNoteId = null;
  }

  componentDidMount() {
  }

  componentDidUpdate() {
    if (this.previousNoteId !== this.props.noteId) {
      this.previousNoteId = this.props.noteId;
      if (this.props.noteId !== null) {
        api.get('/notes/'+this.props.noteId).then(res => {
          this.props.detail_note(res);
        })
      }
    }
  }

  render() {
    if (null === this.props.noteId) {
        return null;
    }

    return (
      <div>
        <h4>Note detail</h4>
        <div>
            {/* <h5>Note content</h5> */}
            {this.props.notes.note.content}
        </div>
      </div>
    );
  }
}

const NoteDetailContainer = ReactRedux.connect(
  state => ({
    notes: state.notes
  }),
  createActionDispatchers(notesActionCreators)
)(NoteDetail);

module.exports = NoteDetailContainer;
