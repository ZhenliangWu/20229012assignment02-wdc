const React = require('react');
const ReactRedux = require('react-redux');

const createActionDispatchers = require('../helpers/createActionDispatchers');
const notebooksActionCreators = require('../reducers/notebooks');

const api = require('../helpers/api');

const NoteList = require('./NoteList');

/*
  *** TODO: Build more functionality into the NotebookList component ***
  At the moment, the NotebookList component simply renders the notebooks
  as a plain list containing their titles. This code is just a starting point,
  you will need to build upon it in order to complete the assignment.
*/
class NotebookList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isCreating: false,
      notebookId: null,
      notebook: {
        title: '',
      },
      search: '',
    }

    // this.onNotebookTitleChange.bind(this);
  }

  loadNotebookList() {
    return api.get('/notebooks').then(res => {
      this.props.list_notebooks(res);
    });
  }

  componentDidMount() {
    this.loadNotebookList();
  }

  displayNotes(id) {
    console.log('displayNotes', id);
    this.setState({
      notebookId: id,
    })
  }

  deleteNotebook(id) {
    api.delete('/notebooks/'+id).then(res => {
      this.loadNotebookList();
    })
    if (id === this.state.notebookId) {
      this.displayNotes(null);
    }
  }

  createNotebook(e) {
    api.post('/notebooks', {
      title: this.state.notebook.title,
    }).then(res => {
      this.hideCreateNotebook();
      this.loadNotebookList();
    })
    
    e.preventDefault();
  }

  showCreateNotebook() {
    this.setState({
      isCreating: true,
    })
  }
  hideCreateNotebook() {
    this.setState({
      isCreating: false,
    })
  }
  
  searchNotebooks(e) {
    api.get('/notebooks?s=' + this.state.search).then(res => {
      this.props.list_notebooks(res);
    });
    
    e.preventDefault();
  }

  onNotebookTitleChange(e) {
    console.log(this, e);
    this.setState({
      notebook: {
        title: e.target.value
      }
    });
  }
  
  onNotebookSearchChange(e) {
    console.log(this, e);
    this.setState({
      search: e.target.value
    });
  }

  render() {
    const createNotebookListItem = (notebook) => {
      return (
        <li key={notebook.id} className="my-notebook-list">
          <button onClick={() => this.deleteNotebook(notebook.id)} className="btn btn-danger btn-xs"><i className="fa fa-close"></i></button>
          <a onClick={() => this.displayNotes(notebook.id)}>{notebook.title}</a>
        </li>
      )
    }

    return (
      <div>
        <h2>Notebooks</h2>

        <div className="my-search row">
          <form className="form-inline">
            <div className="form-group my-search-input-wrap">
              <input type="text" className="form-control my-search-input" id="notebookSearch" placeholder="Search notebooks" value={this.state.search} onChange={(e) => this.onNotebookSearchChange(e)} />
            </div>          
            <button onClick={(e) => this.searchNotebooks(e)} className="btn btn-primary">Search</button>
          </form>
        </div>

        {!this.state.isCreating ? <button className="btn btn-primary" onClick={(e) => this.showCreateNotebook(e)}><i className="fa fa-plus"></i> New notebook</button>
        : <div className="input-group">
                <input value={this.state.notebook.title} onChange={(e) => this.onNotebookTitleChange(e)} type="text" className="form-control" id="notebookTitle" placeholder="Notebook title" />
                <span className="input-group-btn">
                  <button className="btn btn-success" onClick={(e) => this.createNotebook(e)}><i className="fa fa-check"></i></button>
                  <button className="btn btn-default" onClick={(e) => this.hideCreateNotebook(e)}>Cancel</button>
                </span>
        </div>
        }
        <ul>
          {this.props.notebooks.data.map(createNotebookListItem)}
        </ul>

        {/* <div className="row">

          <div className="col-sm-6">
            <ul className="list-group">
              {this.props.notebooks.data.map(createNotebookListItem)}
            </ul>
          </div>

          <div className="col-sm-6">
            <form>
              <div className="form-group">
                <label htmlFor="notebookTitle">Notebook title</label>
                <input value={this.state.notebook.title} onChange={(e) => this.onNotebookTitleChange(e)} type="text" className="form-control" id="notebookTitle" placeholder="Notebook title" />
              </div>
              <div className="form-group">
                <button className="btn btn-primary" onClick={(e) => this.createNotebook(e)}>Crete new notebook</button>
              </div>
            </form>
          </div>

        </div> */}

        <NoteList notebookId={this.state.notebookId} />
      </div>
    );
  }
}

const NotebookListContainer = ReactRedux.connect(
  state => ({
    notebooks: state.notebooks
  }),
  createActionDispatchers(notebooksActionCreators)
)(NotebookList);

module.exports = NotebookListContainer;
