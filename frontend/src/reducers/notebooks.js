const _ = require('lodash');
const api = require('../helpers/api');

// Action type constants
/* *** TODO: Put action constants here *** */
const LIST_NOTEBOOKS = "notebook/list_notebooks";

const initialState = {
  data: [
    { id: 100, title: 'From Redux Store: A hard-coded notebook' },
    { id: 101, title: 'From Redux Store: Another hard-coded notebook' },
  ],
  notes: []
};

// Function which takes the current data state and an action,
// and returns a new state
function reducer(state, action) {
  state = state || initialState;
  action = action || {};

  switch(action.type) {
    /* *** TODO: Put per-action code here *** */
    case LIST_NOTEBOOKS: {
      const s = _.assign({}, state);
      s.data = action.data;
      return s;
    }


    default: return state;
  }
}

// Action creators
/* *** TODO: Put action creators here *** */
reducer.list_notebooks = (data) => {
  // action
  return {
    type: LIST_NOTEBOOKS,
    data: data,
  };
}


// Export the action creators and reducer
module.exports = reducer;
