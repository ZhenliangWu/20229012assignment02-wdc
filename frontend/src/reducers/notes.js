const _ = require('lodash');
const api = require('../helpers/api');

// Action type constants
/* *** TODO: Put action constants here *** */
const LIST_NOTES = "note/list_notes";
const DETAIL_NOTE = "note/detail_note";


const initialState = {
  data: [
  ],
  note: {}
};

// Function which takes the current data state and an action,
// and returns a new state
function reducer(state, action) {
  state = state || initialState;
  action = action || {};

  switch(action.type) {
    /* *** TODO: Put per-action code here *** */
    case LIST_NOTES: {
      const s = _.assign({}, state);
      s.data = action.data;
      return s;
    }
    case DETAIL_NOTE: {
      const s = _.assign({}, state);
      s.note = action.data;
      return s;
    }

    default: return state;
  }
}

// Action creators
/* *** TODO: Put action creators here *** */
reducer.list_notes = (data) => {
  // action
  return {
    type: LIST_NOTES,
    data: data,
  };
}
reducer.detail_note = (data) => {
  return {
    type: DETAIL_NOTE,
    data: data,
  }
}

// Export the action creators and reducer
module.exports = reducer;
