const express = require('express');
const _ = require('lodash');
const models = require('../models');
const Note = models.Note;

const router = express.Router();

/* *** TODO: Fill in the API endpoints for notes *** */
router.get('/', (req, res) => {
    Note.findAll().then((notes) => res.json(notes))
    .catch(err => res.status(500).json({ error: err.message }));
});

router.post('/', (req, res) => {
    Note.create({
        title: req.body.title,
        content: req.body.content,
        notebookId: req.body.notebookId,
    }).then((note) => {
        res.json(note);
    }).catch(err => res.status(500).json({ error: err.message }));
});

router.get('/:id', (req, res) => {
    Note.findById(req.params.id).then((note) => {
        res.json(note);
    }).catch(err => res.status(500).json({ error: err.message }));
})

router.delete('/:id', (req, res) => {
    Note.findById(req.params.id).then((note) => {
        return note.destroy();
    }).then((affectedRows) => {
        res.json({});
    }).catch(err => res.status(500).json({ error: err.message }));
});

router.put('/:id', (req, res) => {
    Note.update({
        title: req.body.title,
        content: req.body.content,
        notebookId: req.body.notebookId,
    }, {
        where: {
            id: req.params.id,
        }
    }).then((affectedRows) => {
        return Note.findById(req.params.id);
    }).then((note) => {
        res.json(note);
    }).catch(err => res.status(500).json({ error: err.message }));
});

module.exports = router;
