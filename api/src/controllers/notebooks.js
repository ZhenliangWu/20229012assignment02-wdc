const express = require('express');
const _ = require('lodash');
const models = require('../models');
const Notebook = models.Notebook;
const Note = models.Note;

const router = express.Router();

// Index
router.get('/', (req, res) => {
  const where = {};
  if (req.query.s) {
    where.title = {$like: '%'+req.query.s+'%'};
  }
  models.Notebook.findAll({
    order: [['createdAt', 'DESC']],
    where: where,
  })
    .then(notebooks => res.json(notebooks))
    .catch(err => res.status(500).json({ error: err.message }));
});

/* *** TODO: Fill in the API endpoints for notebooks *** */
router.get('/:bookId', (req, res) => {
  Notebook.findById(req.params.bookId)
  .then(function(notebook){
    console.log(notebook);
    res.json(notebook)
  }).catch(err => res.status(500).json({ error: err.message}));
  // models.Notebook.findAll({ order: [['createdAt', 'DESC']] })
  //   .then(notebooks => res.json(notebooks))
  //   .catch(err => res.status(500).json({ error: err.message }));
});

router.post('/', (req, res) => {

  Notebook.create({
    title: req.body.title,
  }).then(function(notebook) {
    res.json(notebook);
  }).catch(err => res.status(500).json({ error: err.message }));
});

router.delete('/:bookId', (req, res) => {
  Notebook.findById(req.params.bookId).then(notebook => {
    return notebook.destroy();
  }).then(() => {
    res.json({});
  })
  .catch(err => res.status(500).json({ error: err.message }));
});

router.put('/:bookId', (req, res) => {
  Notebook.update({
    // if the title is not given, the model will omit this field updation
    title: req.body.title,
  }, {
    where: {
      id: req.params.bookId,
    }
  }).then(affectedRows => {
    return Notebook.findById(req.params.bookId);
  })
  .then(notebooks => res.json(notebooks))
  .catch(err => res.status(500).json({ error: err.message }));
})

router.get('/:bookId/notes', (req, res) => {
  const where = {
    notebookId: req.params.bookId,
  };
  if (req.query.s) {
    where.$or = [
      {
        title: {$like: '%'+req.query.s+'%'},
      },
      {
        content: {$like: '%'+req.query.s+'%'},
      },
    ];
  }
  Note.findAll({
    where: where
  }).then((notes) => res.json(notes))
  .catch(err => res.status(500).json({ error: err.message }));
});

module.exports = router;
